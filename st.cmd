epicsEnvSet IOC PBI-DPL01:Ctrl-ECAT-100
epicsEnvSet P LEBT-010:PBI-Dpl-001

require essioc
require ecmccfg 8.0.0

iocshLoad "${ecmccfg_DIR}/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EK1100
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL1808
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL2819

ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput01,1)"  # Feed to limit switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput08,1)"  # Feed to collision avoidance

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL3312
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${E3_CMD_TOP}/hw/configureSlaveLocal.cmd" "HW_DESC=EL7041-0052, CONFIG=-Motor-Nanotec-AS4118L1804"

ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x32,1,1)"  # input 1 hardware interlock

iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis1.ax, DEV=${P}:"

dbLoadRecords "${E3_CMD_TOP}/db/collAvoid.db" "P=${P}:m1-, IOC=${IOC}:"

dbLoadRecords "${E3_CMD_TOP}/db/aliases.db" "P=${P}:, IOC=${IOC}:"
set_pass1_restoreFile "${E3_CMD_TOP}/autosave/default_values.sav" "P=${P}:"
iocshLoad "${essioc_DIR}/common_config.iocsh"

ecmcConfigOrDie "Cfg.EcApplyConfig(1)"
iocshLoad "${ecmccfg_DIR}/setAppMode.cmd"
